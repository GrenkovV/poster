const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copy('node_modules/tinymce/skins', 'public/js/tinymce/skins')
   .copy('node_modules/tinymce/plugins', 'public/js/tinymce/plugins')
   .copy('node_modules/tinymce/icons', 'public/js/tinymce/icons')
   .copy('node_modules/tinymce/themes', 'public/js/tinymce/themes');

mix.js('resources/js/app.js', 'public/js')
   .js('resources/js/blog-theme/clean-blog.min.js', 'public/js')
   .vue()
   .sass('resources/sass/app.scss', 'public/css');
