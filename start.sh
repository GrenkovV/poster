#!/bin/bash


docker-compose down

docker-compose build app

docker-compose up -d

./clear.sh

docker-compose exec app php artisan config:cache

docker-compose exec app php artisan migrate:refresh --seed

#docker-compose exec app php artisan test

