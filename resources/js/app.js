/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

import { BootstrapVue } from 'bootstrap-vue'

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('modal-dialog', require('./components/Modal.vue').default);
Vue.component('simple-confirmation-dialog', require('./components/SimpleConfirmationModal.vue').default);
Vue.component('delete-post-confirmation', require('./components/DeletePostConfirmation.vue').default);
Vue.component('phone-input', require('./components/PhoneInput.vue').default);
Vue.component('likes-component', require('./components/LikesComponent.vue').default);

// Install BootstrapVue
Vue.use(BootstrapVue)
Vue.prototype.$redirect = function (url) {
    window.location.assign(url);
};

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
