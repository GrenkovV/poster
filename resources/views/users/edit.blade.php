@extends('layout')

@section('title')
  {{ $title ?? 'Edit profile' }}
@endsection

@section('content')

  <article>
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">

        <form method="POST" action="{{ route('users.update', ['user' => $user]) }}" enctype="multipart/form-data">
          {{ csrf_field() }}
          {{ method_field('PATCH') }}
          <div class="form-group">
            <label for="first_name">{{ __('First Name') }}</label>
            <input id="first_name" type="text" name="first_name" class="form-control"
                   maxlength="255" value="{{ old('first_name') ?? $user->first_name }}" required>
            @error('first_name')
              <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>
          <div class="form-group">
            <label for="last_name">{{ __('Last Name') }}</label>
            <input id="last_name" type="text" name="last_name" class="form-control"
                   maxlength="255" value="{{ old('last_name') ?? $user->last_name }}" required>
            @error('last_name')
              <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>
          <div class="form-group">
            <phone-input input-id="phone"
                         input-name="phone"
                         input-label="{{ __('Phone Number') }}"
                         input-value="{{ old('phone') ?? $user->phone  }}">
            </phone-input>
            @error('phone')
              <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>

          <div class="form-group">
            <label for="photo">{{ __('Photo') }}</label>
            <input id="photo" type="file" class="form-control-file" name="photo">
            @error('photo')
              <small class="text-danger">{{ $message }}</small>
            @enderror
            @if(!empty($user->photo))
              <img class="mt-3" src="{{ $user->photo_url }}" width="100" height="100">
            @endif
            <hr>
          </div>

          <button type="submit" class="btn btn-primary">Update</button>
        </form>

      </div>
    </div>
  </article>

@endsection