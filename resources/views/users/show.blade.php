@extends('layout')

@section('title')
  {{ $title ?? 'Profile of ' . $user->name }}
@endsection

@section('content')

  <div class="container">
    <dl class="row">
      <dt class="offset-2 col-sm-3 text-right"></dt>
      <dd class="col-md-7 mb-5"><img class="profile-photo" src="{{ $user->photo_url }}" width="200" height="200" alt="Profile photo"></dd>
      <dt class="offset-2 col-sm-3 text-right">{{ __('Name') }}</dt>
      <dd class="col-md-7">{{ $user->name }}</dd>
      <dt class="offset-2 col-sm-3 text-right">{{ __('Email') }}</dt>
      <dd class="col-md-7">{{ $user->email }}</dd>
      <dt class="offset-2 col-sm-3 text-right">{{ __('Phone Number') }}</dt>
      <dd class="col-md-7">{{ $user->phone }}</dd>
    </dl>
    <hr>
    <dl class="row">
      <dt class="offset-2 col-sm-3 text-right">{{ __('Likes Gained') }}</dt>
      <dd class="col-md-7">{{ $user->total_likes }}</dd>
      <dt class="offset-2 col-sm-3 text-right">{{ __('Dislikes Gained') }}</dt>
      <dd class="col-md-7">{{ $user->total_dislikes }}</dd>
    </dl>
  </div>

@endsection