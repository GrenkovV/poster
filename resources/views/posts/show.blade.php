@extends('layout')

@section('title')
  {{ $title ?? $post->title }}
@endsection

@section('content')

  <article>
    <div class="post-preview">
      <div class="container">
        @can('update', $post)
          <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto mb-3">
              <a class="btn btn-primary btn-sm float-left" href="{{ route('posts.edit', ['post' => $post]) }}">Edit</a>
              @can('delete', $post)
                <a class="btn btn-danger btn-sm float-right" v-b-modal="'delete-post-modal'">Delete</a>
              @endcan
            </div>
          </div>
        @endcan
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            {!! $post->body !!}
          </div>
        </div>
        <div class="row mt-3">
          <div class="col-lg-8 col-md-10 mx-auto">
            <p class="post-meta">Posted by
              <a href="{{ route('posts.user', $post->author) }}">
                @if(auth()->check() && $post->author->is(auth()->user()))
                  <b>Me</b>
                @else
                  {{ $post->author->name }} (<a href="{{ route('users.show', $post->author) }}" target="_blank">view profile</a>)
                @endif
              </a>
              on {{ $post->created_at }}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            @can('like', $post)
              <likes-component action="{{ route('posts.like', ['post' => $post]) }}"
                               :likes-count="{{ $post->likes_count }}"
                               :dislikes-count="{{ $post->dislikes_count }}"
                               :liked="{{ $post->dislikedBy(auth()->user()) ? -1 : ($post->likedBy(auth()->user()) ? 1 : 0) }}">
                <div slot="hidden">
                  {{csrf_field()}}
                </div>
              </likes-component>
            @else
              <likes-component :view-mode="true"
                               :likes-count="{{ $post->likes_count }}"
                               :dislikes-count="{{ $post->dislikes_count }}">
              </likes-component>
            @endcan
          </div>
        </div>
      </div>
    </div>
  </article>

  @can('delete', $post)
    <delete-post-confirmation action="{{ route('posts.destroy', ['post' => $post]) }}"
                              post-title="{{ $post->title }}">
      <div slot="hidden">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
      </div>
    </delete-post-confirmation>
  @endcan

@endsection