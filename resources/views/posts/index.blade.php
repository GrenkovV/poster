@extends('layout')

@section('title')
  <div class="container">
    <div class="row">
      @if(Route::current()->getName() === 'posts.user')
        <div class="col-md-3 mb-3 align-self-center">
          <img class="profile-photo" src="{{ $user->photo_url }}" width="120" height="120">
        </div>
        <div class="col-md-9 align-self-center">
          {{ $title ?? 'Recent posts' }}
        </div>
      @else
        <div class="col-12 align-self-center">
          {{ $title ?? 'Recent posts' }}
        </div>
      @endif
    </div>
  </div>
@endsection

@section('content')

  @if(auth()->check())
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto my-2">
        @can('create', \App\Models\Post::class)
          <a class="btn btn-primary" href="{{ route('posts.create') }}">Make a Post</a>
        @endcan
        @if(empty($user) || auth()->user()->isNot($user))
          @if(!empty($i_like_route))
            <a class="btn btn-primary float-right" href="{{ $i_like_route }}">Show posts I liked</a>
          @elseif(!empty($all_posts_route))
            <a class="btn btn-primary float-right" href="{{ $all_posts_route }}">Show all posts</a>
          @endif
        @endif
      </div>
    </div>
  @endif
  <div class="row">
    <div class="col-lg-8 col-md-10 mx-auto">
      @if(!empty($i_like))
        <span class="badge badge-info badge-pill text-white float-right">Liked by me.</span>
      @endif
      @forelse($posts as $post)
        <div class="post-preview">
          <a href="{{ route('posts.show', ['post' => $post]) }}">
            <h2 class="post-title">
              {{ $post->title }}
            </h2>
            <h3 class="post-subtitle">
                {{ $post->teaser }}
            </h3>
          </a>
          <p class="post-meta">Posted by
            <a href="{{ route('posts.user', $post->author->id) }}">
              @if(auth()->check() && $post->author->is(auth()->user()))
                <b>Me</b>
              @else
                {{ $post->author->name }} (<a href="{{ route('users.show', $post->author) }}" target="_blank">view profile</a>)
              @endif
            </a>
            on {{ $post->created_at }}
          </p>
          @can('like', $post)
            <likes-component action="{{ route('posts.like', ['post' => $post]) }}"
                             :likes-count="{{ $post->likes_count }}"
                             :dislikes-count="{{ $post->dislikes_count }}"
                             :liked="{{ $post->dislikedBy(auth()->user()) ? -1 : ($post->likedBy(auth()->user()) ? 1 : 0) }}">
              <div slot="hidden">
                {{csrf_field()}}
              </div>
            </likes-component>
          @else
            <likes-component :view-mode="true"
                             :likes-count="{{ $post->likes_count }}"
                             :dislikes-count="{{ $post->dislikes_count }}">
              <div slot="hidden">
                {{csrf_field()}}
              </div>
            </likes-component>
          @endcan
        </div>
        <hr>
      @empty
        <p class="mt-5"><i>Looks like feed is empty... You are welcome to make a first post!</i></p>
      @endforelse
    <!-- Pager -->
      {{ $posts->links() }}
    </div>
  </div>

@endsection