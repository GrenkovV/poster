@extends('layout')

@section('title')
  {{ $title ?? 'New post' }}
@endsection

@section('content')

  <article>
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">

        <form method="POST" action="{{ route('posts.store') }}">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="title">Title</label>
            <input id="title" type="text" name="title" class="form-control" placeholder="Post title" maxlength="255"
                   value="{{ old('title') }}">
            @error('title')
              <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>
          <div class="form-group">
            <label for="body">Body</label>
            <textarea id="body" class="form-control tinymce" name="body" rows="15">{{ old('body') }}</textarea>
            @error('body')
              <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>

      </div>
    </div>
  </article>

@endsection