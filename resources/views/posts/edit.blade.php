@extends('layout')

@section('title')
  {{ $title ?? 'Edit post' }}
@endsection

@section('content')

  <article>
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">

        <form method="POST" action="{{ route('posts.update', ['post' => $post]) }}">
          {{ csrf_field() }}
          {{ method_field('PATCH') }}
          <div class="form-group">
            <label for="title">Title</label>
            <input id="title" type="text" name="title" class="form-control" placeholder="Post title"
                   maxlength="255" value="{{ old('title') ?? $post->title }}">
            @error('title')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>
          <div class="form-group">
            <label for="body">Body</label>
            <textarea id="body" class="form-control tinymce" name="body"
                      rows="15">{{ old('body') ?? $post->body }}</textarea>
            @error('body')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
          <button type="button" class="btn btn-default float-right" v-b-modal="'confirmation-modal'"><i class="fa fa-fw fa-ban"></i> Cancel</button>
        </form>

      </div>
    </div>
  </article>

  <simple-confirmation-dialog title="Cancel editing"
                              message="Are you sure you want to cancel editing of post?"
                              @confirm="$redirect('{{ route('posts.show', $post) }}')">
  </simple-confirmation-dialog>

@endsection