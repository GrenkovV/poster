<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return redirect()->route('posts.index');
});

Route::get('/posts/user/{user}', [PostController::class, 'userPosts'])
  ->name('posts.user');

Route::post('/posts/{post}/like', [PostController::class, 'like'])
  ->name('posts.like');

Route::resource('posts', PostController::class);

Route::resource('users', UserController::class)->only(['show', 'edit', 'update']);

Auth::routes();
