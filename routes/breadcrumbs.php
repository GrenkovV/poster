<?php

// All Posts
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;


// All Posts > Error
Breadcrumbs::for('error', function ($trail) {

  dump(Route::currentRouteName());
  $trail->parent('posts.index');
  $trail->push('Error');
});

// All Posts
Breadcrumbs::for('posts.index', function ($trail) {
  $trail->push('All posts', route('posts.index'));
});

// All Posts > User 'user name' > 'Post title'
// or
// All Posts > 'Post title'
Breadcrumbs::for('posts.show', function ($trail, $post) {
  if ($post->author()->exists()) {
    $trail->parent('posts.user', $post->author);
  } else {
    $trail->parent('posts.index');
  }
  $trail->push($post->title, route('posts.show', ['post' => $post]));
});


// All Posts > User 'user name'
Breadcrumbs::for('posts.user', function ($trail, $user) {

  $title = 'Feed of \'' . $user->name . '\'';
  if ($user->is(auth()->user())) {
    $title = 'My feed';
  }

  $trail->parent('posts.index');
  $trail->push($title, route('posts.user', ['user' => $user]));
});

// All Posts > New post
Breadcrumbs::for('posts.create', function ($trail) {
  $trail->parent('posts.index');
  $trail->push('New post', route('posts.index'));
});

// All Posts > My feed > Edit post 'Post name'
Breadcrumbs::for('posts.edit', function ($trail, $post) {

  $user = auth()->user();

  $trail->parent('posts.user', $user);
  $trail->push('Edit post \'' . $post->title . '\'', route('posts.index'));
});

// Profile 'User name'
Breadcrumbs::for('users.show', function ($trail, $user) {

  $trail->push('Profile \'' . $user->name . '\'', route('users.show', $user));
});

// Profile 'User name' > Edit
Breadcrumbs::for('users.edit', function ($trail, $user) {

  $trail->parent('users.show', $user);
  $trail->push('Edit profile', route('users.edit', $user));
});
