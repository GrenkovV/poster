<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
  /**
   * Register any application services.
   *
   * @return void
   */
  public function register() {
    //
  }

  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot() {

    // Override default pagination templates.
    Paginator::defaultView('pagination.bootstrap-4');

    Paginator::defaultSimpleView('pagination.simple-bootstrap-4');
  }
}
