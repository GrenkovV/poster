<?php

namespace App\Providers;

use App\Composers\BreadcrumbsComposer;
use App\Composers\PostsViewComposer;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\View;

class ViewServiceProvider extends ServiceProvider
{
  /**
   * Register any application services.
   *
   * @return void
   */
  public function register() {
    //
  }

  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot() {
    // Register posts composer.
    View::composer('posts.*', PostsViewComposer::class);
    View::composer([
      'posts.*',
      'users.*',
    ], BreadcrumbsComposer::class);
  }
}
