<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
  use HasFactory, Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'first_name',
    'last_name',
    'phone',
    'photo',
    'email',
    'password',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password',
    'remember_token',
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'email_verified_at' => 'datetime',
  ];

  /**
   * Relation to posts.
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function posts() {

    return $this->hasMany(Post::class);
  }

  /**
   * Relation to likes.
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function likes() {

    return $this->hasMany(Like::class);
  }

  /**
   * Relation to likes through posts.
   * (get all likes that user's posts received)
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
   */
  public function likesGained() {

    return $this->hasManyThrough(Like::class, Post::class)
      ->where('likes.operation', Like::LIKE);
  }

  /**
   * Relation to dislikes through posts.
   * (get all dislikes that user's posts received)
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
   */
  public function dislikesGained() {

    return $this->hasManyThrough(Like::class, Post::class)
      ->where('likes.operation', Like::DISLIKE);
  }

  /**
   * Getter for user's full name.
   *
   * @return string
   */
  public function getNameAttribute() {

    return $this->last_name . ', ' . $this->first_name;
  }

  /**
   * Get profile photo url.
   *
   * @return string
   */
  public function getPhotoUrlAttribute() {

    return !empty($this->photo) ? asset($this->photo) : asset('img/user-default.jpg');
  }

  /**
   * Get total likes received by user.
   *
   * @return int
   */
  public function getTotalLikesAttribute() {

    return $this->likesGained()->count();
  }

  /**
   * Get total dislikes received by user.
   *
   * @return int
   */
  public function getTotalDislikesAttribute() {

    return $this->dislikesGained()->count();
  }
}
