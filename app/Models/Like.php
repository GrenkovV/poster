<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
  public const NOOP = 0;
  public const LIKE = 1;
  public const DISLIKE = -1;

  use HasFactory;

  protected $fillable = [
    'user_id',
    'post_id',
    'operation'
  ];

  public $timestamps = null;

  /**
   * Relation to post.
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function post() {
    return $this->belongsTo(Post::class);
  }

  /**
   * Relation to user.
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function user() {
    return $this->belongsTo(User::class);
  }

  /**
   * Create like for provided post and user.
   *
   * @param User $user
   * @param Post $post
   * @param int $operation
   *
   * @throws \Exception
   */
  public static function like(User $user, Post $post, $operation = self::LIKE) {

    if (!in_array($operation, [self::LIKE, self::DISLIKE, self::NOOP])) {
      throw new \Exception("Illegal operation code.");
    }

    if ($operation == self::NOOP) {
      // Remove like/dislike.
      Like::where('user_id', $user->id)->where('post_id', $post->id)->delete();
    } else {
      // Create/update like/dislike.
      Like::updateOrCreate(['user_id' => $user->id, 'post_id' => $post->id],
        ['operation' => $operation]);
    }
  }
}
