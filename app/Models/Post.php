<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Mews\Purifier\Facades\Purifier;

class Post extends Model
{
  use HasFactory;

  protected $fillable = [
    'title',
    'body',
  ];

  /**
   * Mutator for body field.
   *
   * @param $value
   */
  public function setBodyAttribute($value) {

    // Clean possible html malicious tags.
    $this->attributes['body'] = Purifier::clean($value);
  }

  /**
   * Relation to User model.
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function author() {
    return $this->belongsTo(User::class, 'user_id', 'id');
  }

  /**
   * Relation to Like model.
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function likes() {
    return $this->hasMany(Like::class);
  }

  /**
   * Get count of likes for this post.
   *
   * @return int
   */
  public function getLikesCountAttribute() {
    return $this->likes()->where('operation', Like::LIKE)->count();
  }

  /**
   * Get count of dislikes for this post.
   *
   * @return int
   */
  public function getDislikesCountAttribute() {
    return $this->likes()->where('operation', Like::DISLIKE)->count();
  }

  /**
   * Convert full body to teaser.
   *
   * @return string
   */
  public function getTeaserAttribute() {

    return Str::words(strip_tags($this->body), 30, '...');
  }

  /**
   * Determine if provided user liked this post.
   *
   * @param User $user
   *
   * @return bool
   */
  public function likedBy(User $user): bool {

    if (empty($user)) {
      return false;
    }

    return $this->likes()
      ->where('user_id', $user->id)
      ->where('operation', Like::LIKE)
      ->exists();
  }

  /**
   * Determine if provided user disliked this post.
   *
   * @param User $user
   *
   * @return bool
   */
  public function dislikedBy(User $user): bool {

    if (empty($user)) {
      return false;
    }

    return $this->likes()
      ->where('user_id', $user->id)
      ->where('operation', Like::DISLIKE)
      ->exists();
  }
}
