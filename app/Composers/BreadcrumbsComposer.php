<?php

namespace App\Composers;

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\View\View;

class BreadcrumbsComposer
{

  /**
   * Create a new view composer.
   *
   * @return void
   */
  public function __construct() {
  }

  /**
   * Bind data to the view.
   *
   * @param \Illuminate\View\View $view
   * @return void
   */
  public function compose(View $view) {

    // Breadcrumbs processing.
    $breadcrumbs = Breadcrumbs::exists()
      ? Breadcrumbs::render()
      : Breadcrumbs::render('error');

    $view->with('breadcrumbs', $breadcrumbs);
  }
}