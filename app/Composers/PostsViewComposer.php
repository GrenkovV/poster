<?php

namespace App\Composers;

use Illuminate\View\View;

class PostsViewComposer
{

  /**
   * Create a new view composer.
   *
   * @return void
   */
  public function __construct() {
  }

  /**
   * Bind data to the view.
   *
   * @param \Illuminate\View\View $view
   * @return void
   */
  public function compose(View $view) {

    $i_like = request()->query->has('i_like');
    // Get route with 'i_like' parameter for link.
    $i_like_route = $i_like ? null : request()->fullUrlWithQuery(['i_like' => 1]);
    $all_posts_route = request()->fullUrlWithQuery(['i_like' => null]);

    $viewData = $view->getData();

    // Title processing.
    $title = $viewData['title'] ?? null;
    if (array_key_exists('post', $viewData) && !empty($viewData['post'])
      && !array_key_exists('title', $viewData)) {

      $title = $viewData['post']->title;
    }

    $view->with('title', $title)
      ->with('i_like', $i_like)
      ->with('i_like_route', $i_like_route)
      ->with('all_posts_route', $all_posts_route);
  }
}