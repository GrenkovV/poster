<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Laracasts\Flash\Flash;

class UserController extends Controller
{
  /**
   * Display the specified resource.
   *
   * @param \App\Models\User $user
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
   */
  public function show(User $user) {

    $this->authorize('view', $user);

    return view('users.show', compact('user'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param \App\Models\User $user
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
   */
  public function edit(User $user) {

    $this->authorize('update', $user);

    return view('users.edit', compact('user'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param \App\Models\User $user
   *
   * @return \Illuminate\Http\RedirectResponse
   *
   * @throws ValidationException
   */
  public function update(Request $request, User $user) {

    $this->authorize('update', $user);

    $validated = $request->validate([
      'first_name' => 'required|max:255',
      'last_name' => 'required|max:255',
      'phone' => 'nullable|regex:/^\(\d{3}\)\s\d{3}-\d{4}$/', // format (xxx) xxx-xxxx
      'photo' => 'image',
    ], [
      'phone.regex' => 'Phone number has invalid format.'
    ]);

    // Process user photo.
    if ($request->has('photo')) {

      $oldPhoto = $user->photo;

      // Save new photo.
      $validated['photo'] = $validated['photo']->store('avatars');
      if (!$validated['photo']) {
        throw ValidationException::withMessages([
          'photo' => ['Failed to save photo.'],
        ]);
      }

      // Remove old profile photo.
      if (!empty($oldPhoto)) {
        Storage::delete($oldPhoto);
      }
    }

    $user->update($validated);

    Flash()->success('Profile updated.');

    return redirect()->route('posts.index');
  }

}
