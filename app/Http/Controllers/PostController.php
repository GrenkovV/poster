<?php

namespace App\Http\Controllers;

use App\Models\Like;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class PostController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
   */
  public function index(Request $request) {

    $this->authorize('viewAny', Post::class);

    $query = Post::with(['author', 'likes'])
      ->whereHas('author');

    if (auth()->check() && $request->has('i_like')) {
      $currentUser = auth()->user();
      $query->whereHas('likes', function ($query) use ($currentUser) {
        $query->where('user_id', $currentUser->id)->where('operation', Like::LIKE);
      });
    }

    $posts = $query->orderByDesc('created_at')
      ->simplePaginate(config('poster.page_size'))
      ->appends($request->query());

    return view('posts.index', compact('posts'));
  }

  /**
   * Display a listing of posts for specified user.
   *
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
   */
  public function userPosts(Request $request, User $user) {

    $this->authorize('viewAny', Post::class);

    $query = Post::with(['author', 'likes'])
      ->where('user_id', $user->id);

    if ($user->isNot(auth()->user()) && $request->has('i_like')) {
      $currentUser = auth()->user();
      $query->whereHas('likes', function ($query) use ($currentUser) {
        $query->where('user_id', $currentUser->id)->where('operation', Like::LIKE);
      });
    }

    $posts = $query->orderByDesc('created_at')
      ->simplePaginate(config('poster.page_size'))
      ->appends($request->query());

    $title = $user->is(auth()->user())
      ? 'My feed'
      : 'Feed of ' . $user->name;

    return view('posts.index', compact('posts', 'user', 'title'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
   */
  public function create() {

    $this->authorize('create', Post::class);

    return view('posts.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\RedirectResponse
   */
  public function store(Request $request) {

    $this->authorize('create', Post::class);

    $validated = $request->validate([
      'title' => 'required|max:255',
      'body' => 'required',
    ]);

    $post = new Post($validated);
    auth()->user()->posts()->save($post);

    return redirect()->route('posts.show', compact('post'));
  }

  /**
   * Display the specified resource.
   *
   * @param \App\Models\Post $post
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
   */
  public function show(Post $post) {

    $this->authorize('view', $post);

    return view('posts.show', compact('post'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param \App\Models\Post $post
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
   */
  public function edit(Post $post) {

    $this->authorize('update', $post);

    $title = 'Edit post \'' . $post->title . '\'';

    return view('posts.edit', compact('post', 'title'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param \App\Models\Post $post
   * @return \Illuminate\Http\RedirectResponse
   */
  public function update(Request $request, Post $post) {

    $this->authorize('update', $post);

    $validated = $request->validate([
      'title' => 'required|max:255',
      'body' => 'required',
    ]);

    $post->update($validated);

    return redirect()->route('posts.show', $post);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param \App\Models\Post $post
   * @return \Illuminate\Http\RedirectResponse
   */
  public function destroy(Post $post) {

    $this->authorize('delete', $post);

    $post->delete();

    return redirect()->route('posts.index');
  }

  /**
   * Like/dislike a post.
   *
   * @param Request $request
   * @param Post $post
   *
   * @return \Illuminate\Http\RedirectResponse
   *
   * @throws \Illuminate\Auth\Access\AuthorizationException
   */
  public function like(Request $request, Post $post) {

    $this->authorize('like', $post);

    $request->validate([
      'operation' => 'required|in:' . implode(',', [Like::LIKE, Like::DISLIKE, Like::NOOP]),
    ], [
      'operation.in' => 'Operation has illegal value.',
    ]);

    Like::like(auth()->user(), $post, $request->post('operation'));

    return redirect()->back();
  }
}
