<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
  use RefreshDatabase;


  protected function getRoutes($user) {
    return [
      'users.show'   => ['params' => ['user' => $user], 'guest_status' => 200],
      'users.edit'   => ['params' => ['user' => $user]],
      'users.update' => ['method' => 'patch', 'params' => ['user' => $user]],
    ];
  }

  public function test_access_routes() {

    $user = User::factory()->create();
    $routes = $this->getRoutes($user);

    $this->assertAuthenticationRequiredForRoutes($routes);
  }

  /**
   * .
   *
   * @return void
   */
  public function test_can_see_user_info() {

    $user = User::factory()->create();

    $response = $this->get(route('users.show', ['user' => $user->id]));

    $response->assertViewIs('users.show')
      ->assertSeeText($user->name)
      ->assertSeeText($user->email)
      ->assertSeeText($user->phone)
      ->assertSee($user->photo_url)
      ->assertStatus(200);
  }

  /**
   * .
   *
   * @return void
   */
  public function test_cannot_see_profile_edit_form_of_different_user() {

    $user = User::factory()->create();
    $userDifferent = User::factory()->create();

    $response = $this->actingAs($user)->get(route('users.edit', ['user' => $userDifferent->id]));

    $response->assertStatus(403);
  }

  /**
   * .
   *
   * @return void
   */
  public function test_can_see_profile_edit_form() {

    $user = User::factory()->create();

    $response = $this->actingAs($user)->get(route('users.edit', ['user' => $user->id]));

    $response->assertViewIs('users.edit')
      ->assertViewHas('user')
      ->assertSeeInOrder([
        'First Name',
        $user->first_name,
        'Last Name',
        $user->last_name,
        'Phone Number',
        $user->phone,
      ])
      ->assertStatus(200);
  }

  /**
   * User profile can be updated with route.
   *
   * @return void
   */
  public function test_can_update_profile_with_form() {

    $user = User::factory()->create();

    $updateData = [
      'first_name' => 'First Name #1',
      'last_name' => 'Last Name #1',
      'phone' => '(111) 222-3333',
    ];

    $response = $this->actingAs($user)->patch(route('users.update', ['user' => $user]),
      $updateData);

    $user->refresh();

    foreach ($updateData as $key => $value) {
      $this->assertTrue($user->$key === $value);
    }

    $response->assertStatus(302);
  }

  /**
   * User profile photo can be uploaded with route.
   *
   * @return void
   */
  public function test_can_update_profile_photo_with_form() {

    $user = User::factory()->create();

    $updateData = [
      'first_name' => 'First Name #1',
      'last_name' => 'Last Name #1',
      'phone' => '(111) 222-3333',
      'photo' => UploadedFile::fake()->image('test.jpg'),
    ];

    $response = $this->actingAs($user)->patch(route('users.update', ['user' => $user]),
      $updateData);

    $user->refresh();

    $this->assertTrue(!empty($user->photo));

    Storage::assertExists($user->photo);

    $response->assertStatus(302);
  }

  /**
   * User profile photo can be uploaded with route.
   *
   * @return void
   */
  public function test_old_profile_photo_deleted_after_update() {

    $user = User::factory()->create();

    $updateData = [
      'first_name' => 'First Name #1',
      'last_name' => 'Last Name #1',
      'phone' => '(111) 222-3333',
      'photo' => UploadedFile::fake()->image('test.jpg'),
    ];

    $this->actingAs($user)->patch(route('users.update', ['user' => $user]),
      $updateData);

    $user->refresh();

    $this->assertTrue(!empty($user->photo));

    $oldPhoto = $user->photo;

    $updateData['photo'] = UploadedFile::fake()->image('test2.jpg');

    $response = $this->actingAs($user)->patch(route('users.update', ['user' => $user]),
      $updateData);

    $user->refresh();

    $this->assertTrue(!empty($user->photo));

    Storage::assertExists($user->photo);
    Storage::assertMissing($oldPhoto);

    $response->assertStatus(302);
  }

  /**
   * User profile photo can be uploaded with route.
   *
   * @return void
   */
  public function test_cannot_update_profile_photo_with_not_image() {

    $user = User::factory()->create();

    $updateData = [
      'first_name' => 'First Name #1',
      'last_name' => 'Last Name #1',
      'phone' => '(111) 222-3333',
      'photo' => UploadedFile::fake()->image('test.xml'),
    ];

    $response = $this->actingAs($user)->patch(route('users.update', ['user' => $user]),
      $updateData);

    $user->refresh();

    $this->assertTrue(empty($user->photo));

    $response->assertSessionHasErrors(['photo']);
    $response->assertStatus(302);
  }

  /**
   * User profile photo can be uploaded with route.
   *
   * @return void
   */
  public function test_update_profile_fields_validation() {

    $user = User::factory()->create();

    $updateData = [
      'phone' => '1112223333',
    ];

    $response = $this->actingAs($user)->patch(route('users.update', ['user' => $user]),
      $updateData);

    $response->assertSessionHasErrors(['first_name', 'last_name', 'phone']);
    $response->assertStatus(302);
  }
}
