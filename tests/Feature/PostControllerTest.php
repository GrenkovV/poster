<?php

namespace Tests\Feature;

use App\Models\Like;
use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostControllerTest extends TestCase
{
  use RefreshDatabase;

  protected function getRoutes($post, $user) {
    return [
      'posts.index'   => ['guest_status' => 200],
      'posts.create'  => ['guest_status' => 403],
      'posts.store'   => ['method' => 'post', 'guest_status' => 403],
      'posts.show'    => ['params' => ['post' => $post], 'guest_status' => 200],
      'posts.edit'    => ['params' => ['post' => $post], 'guest_status' => 403],
      'posts.update'  => ['method' => 'patch', 'params' => ['post' => $post], 'guest_status' => 403],
      'posts.destroy' => ['method' => 'delete', 'params' => ['post' => $post], 'guest_status' => 403],
      'posts.like'    => ['method' => 'post', 'params' => ['post' => $post], 'guest_status' => 403],
      'posts.user'    => ['params' => ['user' => $user], 'guest_status' => 200],
    ];
  }

  /**
   * Generate user and array of posts for this user.
   *
   * @param User|null $user
   * @param int $postsCount
   *
   * @return array
   */
  private function createUserAndPosts(User $user = null, $postsCount = 5): array {

    $user = $user ?? User::factory()->create();
    $posts = Post::factory($postsCount)->create(['user_id' => $user->id]);

    return compact('user', 'posts');
  }

  /**
   * Test response codes for guest user on each of routes.
   */
  public function test_access_routes() {

    $user = User::factory()->create();
    $post = Post::factory()->create(['user_id' => $user->id]);
    $routes = $this->getRoutes($post, $user);

    $this->assertAuthenticationRequiredForRoutes($routes);
  }

  public function test_can_see_posts_in_list() {

    extract($this->createUserAndPosts());

    $response = $this->get(route('posts.index'));

    $response->assertViewIs('posts.index')
      ->assertSeeText($user->name)
      ->assertSee(route('users.show', $user));

    collect($posts)->each(function ($post) use ($response) {
      $response->assertSeeText($post->teaser)
        ->assertSeeText($post->title);
    });

    $response->assertStatus(200);
  }

  /**
   * Check that on 'posts.user' route posts displayed for provided user only and not for the others.
   */
  public function test_can_see_posts_in_user_list() {

    $targetData = $this->createUserAndPosts(null, 3);

    $differentData = $this->createUserAndPosts(null, 3);

    // Test with guest.
    $response = $this->get(route('posts.user', ['user' => $targetData['user']->id]));

    $response->assertViewIs('posts.index')
      ->assertSeeText($targetData['user']->name)
      ->assertDontSeeText($differentData['user']->name)
      ->assertSee(route('users.show', $targetData['user']))
      ->assertDontSee(route('users.show', $differentData['user']));

    collect($targetData['posts'])->each(function ($post) use ($response) {
      $response->assertSeeText($post->teaser)
        ->assertSeeText($post->title);
    });

    collect($differentData['posts'])->each(function ($post) use ($response) {
      $response->assertDontSeeText($post->teaser)
        ->assertDontSeeText($post->title);
    });

    $response->assertStatus(200);

    // ------------------------------------

    // Repeat test with authenticated user.
    $this->authenticateUser();

    $response = $this->get(route('posts.user', ['user' => $targetData['user']->id]));

    $response->assertViewIs('posts.index')
      ->assertSeeText($targetData['user']->name)
      ->assertDontSeeText($differentData['user']->name)
      ->assertSee(route('users.show', $targetData['user']))
      ->assertDontSee(route('users.show', $differentData['user']));

    collect($targetData['posts'])->each(function ($post) use ($response) {
      $response->assertSeeText($post->teaser)
        ->assertSeeText($post->title);
    });

    collect($differentData['posts'])->each(function ($post) use ($response) {
      $response->assertDontSeeText($post->teaser)
        ->assertDontSeeText($post->title);
    });

    $response->assertStatus(200);
  }

  /**
   * Check that guest and authenticated user can read post.
   */
  public function test_can_see_post_by_guest_and_user() {

    $users = User::factory(4)->create();

    $targetData = $this->createUserAndPosts($users[0], 3);

    $post = $targetData['posts'][0];

    Like::like($users[1], $post);
    Like::like($users[2], $post);
    Like::like($users[3], $post, Like::DISLIKE);

    // Test with guest user.
    $response = $this->get(route('posts.show', $post));

    $response->assertViewIs('posts.show')
      ->assertSeeText($users[0]->name)
      ->assertSeeText($post->title)
      ->assertSee($post->body, false)
      ->assertSeeText($post->likes_count)
      ->assertSeeText($post->dislikes_count)
      ->assertDontSee(route('posts.like', $post))
      ->assertSee(route('posts.user', $users[0]))
      ->assertSee(route('users.show', $users[0]));

    $this->authenticateUser($users[1]);

    // Test with authenticated user.
    $response = $this->get(route('posts.show', $post));

    $response->assertViewIs('posts.show')
      ->assertSeeText($users[0]->name)
      ->assertSeeText($post->title)
      ->assertSee($post->body, false)
      ->assertSeeText($post->likes_count)
      ->assertSeeText($post->dislikes_count)
      ->assertSee(route('posts.like', $post))
      ->assertSee(route('posts.user', $users[0]))
      ->assertSee(route('users.show', $users[0]));


    $response->assertStatus(200);
  }

  /**
   * Check that user can like/dislike post and remove previous choice.
   */
  public function test_can_like_dislike_post() {

    $users = User::factory(2)->create();

    $targetData = $this->createUserAndPosts($users[0], 3);

    $post = $targetData['posts'][0];

    $this->authenticateUser($users[1]);

    // Test that user can like post.
    $response = $this->post(route('posts.like', $post), ['operation' => Like::LIKE]);

    self::assertTrue($post->likedBy($users[1]));
    self::assertNotTrue($post->dislikedBy($users[1]));
    $response->assertRedirect();

    // Test that user can dislike post.
    $response = $this->post(route('posts.like', $post), ['operation' => Like::DISLIKE]);

    self::assertNotTrue($post->likedBy($users[1]));
    self::assertTrue($post->dislikedBy($users[1]));
    $response->assertRedirect();

    // Test that user can remove like/dislike mark.
    $response = $this->post(route('posts.like', $post), ['operation' => Like::NOOP]);

    self::assertNotTrue($post->likedBy($users[1]));
    self::assertNotTrue($post->dislikedBy($users[1]));
    $response->assertRedirect();
  }

  /**
   * Check that user can see on index page posts that he liked.
   */
  public function test_user_can_see_liked_posts_on_index() {

    $users = User::factory(2)->create();

    $targetData = $this->createUserAndPosts($users[0], 4);

    $posts = $targetData['posts'];

    Like::like($users[1], $posts[0]);
    Like::like($users[1], $posts[1]);
    Like::like($users[1], $posts[2], Like::DISLIKE);

    $this->authenticateUser($users[1]);

    // Test that user can like post.
    $response = $this->get(route('posts.index', ['i_like' => 1]));

    $response->assertViewIs('posts.index')
      ->assertSee([$posts[0]->title, $posts[1]->title])
      ->assertDontSee([$posts[2]->title, $posts[3]->title])
      ->assertStatus(200);
  }

  /**
   * Check that user can see on user's feed page posts that he liked.
   */
  public function test_user_can_see_liked_posts_on_user_posts_page() {

    $users = User::factory(2)->create();

    $targetData = $this->createUserAndPosts($users[0], 4);

    $posts = $targetData['posts'];

    Like::like($users[1], $posts[0]);
    Like::like($users[1], $posts[1]);
    Like::like($users[1], $posts[2], Like::DISLIKE);

    $this->authenticateUser($users[1]);

    // Test that user can like post.
    $response = $this->get(route('posts.user', ['user' => $users[0], 'i_like' => 1]));

    $response->assertViewIs('posts.index')
      ->assertSee([$posts[0]->title, $posts[1]->title])
      ->assertSeeText('Liked by me')
      ->assertDontSee([$posts[2]->title, $posts[3]->title])
      ->assertStatus(200);
  }

  /**
   * Check that user can not see 'Show that I liked' button on his own feed page.
   */
  public function test_user_cannot_see_show_liked_posts_button_on_my_feed_page() {

    $user = User::factory()->create();

    $this->createUserAndPosts($user, 4);

    $this->authenticateUser($user);

    $response = $this->get(route('posts.user', $user));

    $response->assertViewIs('posts.index')
      ->assertDontSee('Show posts I liked')
      ->assertStatus(200);
  }

  /**
   * Check that user can see 'Show posts I liked' button on someone else feed page (or index).
   */
  public function test_user_can_see_show_liked_posts_button_on_other_user_feed_page() {

    $users = User::factory(2)->create();

    $this->createUserAndPosts($users[0], 4);

    $this->authenticateUser($users[1]);

    // Test index page.
    $response = $this->get(route('posts.index'));

    $response->assertViewIs('posts.index')
      ->assertSee('Show posts I liked')
      ->assertStatus(200);

    // Test user feed page.
    $response = $this->get(route('posts.user', $users[0]));

    $response->assertViewIs('posts.index')
      ->assertSee('Show posts I liked')
      ->assertStatus(200);
  }

  /**
   * Check that guest can not see 'Show posts I liked' button.
   */
  public function test_guest_cannot_see_show_liked_posts_button_on_other_user_feed_page() {

    $user = User::factory()->create();

    $this->createUserAndPosts($user, 2);

    // Test index page.
    $response = $this->get(route('posts.index'));

    $response->assertViewIs('posts.index')
      ->assertDontSee('Show posts I liked')
      ->assertStatus(200);

    // Test user feed page.
    $response = $this->get(route('posts.user', $user));

    $response->assertViewIs('posts.index')
      ->assertDontSee('Show posts I liked')
      ->assertStatus(200);
  }

  /**
   * Check that user can create new post.
   */
  public function test_user_can_create_post() {

    $postData = Post::factory()->makeOne()->attributesToArray();

    $user = $this->authenticateUser();

    // Test index page.
    $response = $this->post(route('posts.store'), $postData);

    $this->assertTrue($user->posts()->count() === 1);

    $response->assertRedirect();
  }

  /**
   * Check that user can edit post.
   */
  public function test_user_can_edit_post() {

    $postData = Post::factory()->makeOne()->attributesToArray();

    $user = $this->authenticateUser();

    $post = Post::factory()->create(['user_id' => $user->id]);

    // Test index page.
    $response = $this->patch(route('posts.update', $post), $postData);

    $post->refresh();

    $this->assertTrue($post->title === $postData['title']);
    $this->assertTrue($post->body === $postData['body']);

    $response->assertRedirect();
  }

  /**
   * Check that user can delete post.
   */
  public function test_user_can_delete_post() {

    $user = $this->authenticateUser();

    $post = Post::factory()->create(['user_id' => $user->id]);

    // Test index page.
    $response = $this->delete(route('posts.destroy', $post));

    $this->assertNotTrue(Post::where('id', $post->id)->exists());

    $response->assertRedirect();
  }
}
