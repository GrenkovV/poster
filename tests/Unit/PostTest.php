<?php

namespace Tests\Unit;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PostTest extends TestCase
{

  use RefreshDatabase;

  /**
   * Test that Post body filtered correctly.
   *
   * @return void
   */
  public function test_post_body_filtered() {

    $body = '<h1>text_data_1<strong>text_data_2</strong><script>alert</script></h1>';
    $post = Post::factory()->create(['body' => $body]);

    $this->assertDatabaseCount('posts', 1);
    $this->assertDatabaseHas('posts', ['title' => $post->title]);
    $this->assertTrue(str_contains($post->body, '<h1>'));
    $this->assertTrue(str_contains($post->body, '<strong>'));
    $this->assertTrue(str_contains($post->body, 'text_data_1'));
    $this->assertTrue(str_contains($post->body, 'text_data_2'));
    $this->assertNotTrue(str_contains($post->body, 'script>'));
    $this->assertNotTrue(str_contains($post->body, 'alert'));
  }
}
