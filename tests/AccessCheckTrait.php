<?php


namespace Tests;


use App\Models\User;
use InvalidArgumentException;

trait AccessCheckTrait
{
  /**
   * Authenticate provided or new user for further requests.
   *
   * @param User|null $user
   * @param array $attributes
   *
   * @return User|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed|null
   */
  protected function authenticateUser(User $user = null, array $attributes = []) {

    if (empty($user)) {
      $user = User::factory()->create($attributes);
    }

    // Authenticate user.
    $this->actingAs($user);

    return $user;
  }

  /**
   * @param $routes
   */
  protected function assertAuthenticationRequiredForRoutes($routes) {

    foreach ($routes as $routeName => $routeData) {
      $params = $routeData['params'] ?? [];
      $method = $routeData['method'] ?? 'get';
      $guestStatus = $routeData['guest_status'] ?? 403;

      $this->assertAuthenticationRequired(route($routeName, $params), $method, $guestStatus);
    }
  }

  /**
   * @param $uri
   * @param string $method
   * @param int $guestStatus
   */
  protected function assertAuthenticationRequired($uri, $method = 'get', $guestStatus = 403) {
    $method = strtolower($method);
    if (!in_array($method, ['get', 'post', 'patch', 'put', 'update', 'delete'])) {
      throw new InvalidArgumentException('Invalid method: ' . $method);
    }

    $response = $this->$method($uri);
    $response->assertStatus($guestStatus);
  }

}