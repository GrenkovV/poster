## About Poster

Poster is a sample blog application with ability to register, login, view posts in guest and authenticated mode.

### Prerequisites for project
- installed `docker` without sudo requirement (https://docs.docker.com/engine/install/ubuntu/ , https://docs.docker.com/engine/install/linux-postinstall/).
- installed `docker-compose` (https://docs.docker.com/compose/install/).
- install `git`.

### Simple steps to get project up and running

Make directory on local machine and run in console:

1. `git clone https://bitbucket.org/GrenkovV/poster.git`
2. `cd poster && cp .env.docker .env`
3. `docker-compose build app`
4. `docker-compose up -d`
5. `docker-compose exec app composer install`
6. `docker-compose exec app php artisan key:generate`
7. `docker-compose exec app php artisan migrate --seed`
8. `docker-compose exec app php artisan storage:link`
9. `docker-compose exec app php artisan test`

Now you have running project with some generated posts and users.
Feel free to register new user and discover.

### Some notes

For access control Poster utilizes Policies.

You can change default page size in `.env` file (PAGE_SIZE=...) or in `config/poster.php`. Project uses free [Clean Blog](https://startbootstrap.com/theme/clean-blog) Bootstrap 4 theme and for several components - `Vue.js`.

